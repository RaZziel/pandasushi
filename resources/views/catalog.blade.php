@extends('layouts.main')
@section('mobile_filter_block')
    <div id="mobile_filter_block">
        <a class="close-window" id="close_filter_menu" href="javascript:void(0);">
            <span class="fa fa-window-close"></span>
        </a>
        <div class="catalog_title">
            <p>@lang('web.product_categories')</p>
        </div>
        <ul>
            @foreach($catalog_categories as $catalog_category)
                <li>
                    <input type="hidden" value="0" id="{{ $catalog_category->category_id }}" />
                    <label>
                        <span class="checkbox"><i></i></span>
                        <?php $category_name = 'category_name_'.$locale; ?>
                        {{ $catalog_category->$category_name }}
                    </label>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
@section('lang')
    <li>
        <ul class="lang">
            <li>
                <a href="/ru/catalog">RU</a>
            </li>
            <li>
                <a href="/en/catalog">EN</a>
            </li>
            <li>
                <a href="/ro/catalog">RO</a>
            </li>
        </ul>
    </li>
@endsection
@section('content')
    <div id="content">
        <ul class="lang">
            <li>
                <a href="/ru/catalog">RU</a>|
            </li>
            <li>
                <a href="/en/catalog">EN</a>|
            </li>
            <li>
                <a href="/ro/catalog">RO</a>
            </li>
        </ul>
        <div class="content-block">
            <div id="catalog_menu">
                <div class="catalog_title">
                    <p>@lang('web.product_categories')</p>
                </div>
                <ul>
                    @foreach($catalog_categories as $catalog_category)
                        <li>
                            <input type="hidden" value="0" id="{{ $catalog_category->category_id }}" />
                            <label>
                                <span class="checkbox"><i></i></span>
                                <?php $category_name = 'category_name_'.$locale; ?>
                                {{ $catalog_category->$category_name }}
                            </label>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div id="catalog_sushi">
                <div class="catalog_title">
                    <p>@lang('web.products')</p>
                </div>
                <div id="sushi_blocks">
                    @foreach($catalog as $catalog_item)
                        <?php
                            $catalog_description = 'catalog_description_'.$locale;
                        ?>
                        <div data-id="{{ $catalog_item->catalog_id }}" class="sushi_block">
                            <img src="{{ $catalog_item->upload_image_path }}" />
                            <div class="sushi_block_text">
                            <span class="sushi_text">
                                {{ $catalog_item->catalog_title }}
                            </span>
                            <span class="sushi_description">
                                {{ $catalog_item->$catalog_description }}
                            </span>
                            <span @if($catalog_item->hot_price_status) style="color:#c45f5f" @endif class="sushi_price">
                                {{ $catalog_item->catalog_price }} @lang('web.lei')
                            </span>
                                <a href="javascript:void(0);" @if(in_array($catalog_item->catalog_id,$cart_items)) class="add_sushi in_cart" @else class="add_sushi" @endif>
                                    <span class="fa fa-plus-square"></span>
                                </a>
                                <a href="javascript:void(0);" class="checked_sushi">
                                    <span class="fa fa-check-square"></span>
                                </a>
                                <a href="javascript:void(0);" class="remove_sushi">
                                    <span class="fa fa-window-close"></span>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                @if(count($catalog) > 0)
                    <div class="content-block" id="catalog_see_more">
                        <a href="javascript:void(0)" >@lang('web.see_more')</a>
                    </div>
                @else
                    <h3 align="center">@lang('web.catalog_is_empty')</h3><br/><br/><br/>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            var cart_count = {{ count($cart_items) }};
            var catalog_categories = [];
            var limit = {{ config('ctr.common.limit') }};
            var limit_increment = {{ config('ctr.common.limit') }};

            function setCartCount() {
                $('.cart_count').html(cart_count);
            }

            function setCart(sushi_id,method) {
                $.ajax({
                    url: '/set/' + method,
                    type: 'POST',
                    data: {'_token': '<?=csrf_token();?>','sushi_id': sushi_id},
                    success: function(){},
                    error: function (data) {
                        alert('Error.Try again!');
                    }
                });
            }

            function check_item(object) {
                $(object).css('display','none');
                $(object).siblings('a.checked_sushi').css('display','block').one('mouseout',function () {
                    $(this).mouseenter(function () {
                        $(this).siblings('.remove_sushi').css('display','block');
                        $(this).css('display','none');
                    });

                    $('.sushi_block .remove_sushi').mouseleave(function () {
                        if($(this).siblings('a.add_sushi').css('display') == 'none'){
                            $(this).siblings('a.checked_sushi').css('display','block');
                            $(this).css('display','none');
                        }
                    });
                });
            }

            function set_in_cart() {

                $(".sushi_block .in_cart").each(function (i,v) {
                    check_item(v);
                    var checked_sushi = $(v).siblings('a.checked_sushi');

                    $(checked_sushi).mouseenter(function () {
                        $(this).siblings('.remove_sushi').css('display','block');
                        $(this).css('display','none');
                    });

                });

            }

            function ActivateSushiBlockButtons() {
                $(".sushi_block .add_sushi").click(function () {
                    var sushi_id = $(this).closest('div.sushi_block').attr('data-id');

                    check_item(this);
                    setCart(sushi_id,'add_to_cart');
                    cart_count++;
                    setCartCount();
                });

                $(".sushi_block .remove_sushi").click(function () {
                    var sushi_id = $(this).closest('div.sushi_block').attr('data-id');

                    $(this).parent().find('a').css('display','none').unbind('mouseenter');
                    $(this).siblings('.add_sushi').css('display','block');
                    setCart(sushi_id,'remove_from_cart');
                    cart_count--;
                    setCartCount();
                });
            }

            function refreshCatalog(limit) {

                $.ajax({
                    url: '/{{ $locale }}/get/get_catalog',
                    type: 'POST',
                    data: {'_token': '<?=csrf_token();?>','catalog_categories': catalog_categories,'limit': limit},
                    success: function (data) {
                        if(!data){
                            data = "<h3 align='center'>@lang('web.catalog_is_empty')</h3><br/><br/><br/>";
                            $("#catalog_see_more").css('display','none');
                        }else{
                            $("#catalog_see_more").css('display','block');
                        }

                        $("#catalog_sushi #sushi_blocks").hide().html(data).fadeIn('slow');
                        set_in_cart();
                        console.log($(".sushi_description").val());

                        var items_count = data.split('class="sushi_block"').length;

                        if(items_count < limit){
                            $("#catalog_see_more").css('display','none');
                        }
                        ActivateSushiBlockButtons();
                    }
                });
            }

            set_in_cart();
            ActivateSushiBlockButtons();


            $("#catalog_menu ul li,#mobile_filter_block ul li").click(function () {
                var check_input = $(this).find('input');

                if(parseInt(check_input.val())){
                    check_input.val(0);
                    $(this).find('i').css('display','none');
                    $(this).find('span').css('border','1px solid #acacac');
                    catalog_categories.splice(catalog_categories.indexOf(check_input.attr('id')),1);
                }else{
                    check_input.val(1);
                    $(this).find('i').css('display','block');

                    if($(this).closest('div').attr('id') != 'mobile_filter_block'){
                        $(this).find('span').css('border','1px solid #ffa359');
                    }

                    if(!catalog_categories.includes(check_input.attr('id'))){
                        catalog_categories.push(check_input.attr('id'));
                    }
                }

                refreshCatalog(limit);
            });

            $("#catalog_see_more").click(function () {
                limit = limit + limit_increment;
                refreshCatalog(limit);
            });

            $("#open_filter_menu").click(function (){
                $("#mobile_filter_block").css('transform','translate(-250px,0)');
            });

            $("#close_filter_menu").click(function (){
                $("#mobile_filter_block").css('transform','translate(250px,0)');
            });

        });
    </script>

@endsection
