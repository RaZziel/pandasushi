<html>
    <head>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
        <link rel="stylesheet" href="{{ asset('css/reset.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
        <title>Panda sushi</title>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '320836395062799');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1"
                 src="https://www.facebook.com/tr?id=320836395062799&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108741712-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-108741712-1');
</script>
        @yield('style')
    </head>
    <body>
        <header>
            <div id="logo-block">
                <div class="logo"></div>
            </div>
            <div id="header-text">
                <div id="inner-text">
                    <p id="text-title">
                        @lang('web.header_text_title')
                    </p>
                    <p>
                        @lang('web.header_text')
                    </p>
                </div>
            </div>
            <div id="menu">
                <ul>
                    <li>
                        <a href="/{{ $locale }}">Panda sushi</a>
                    </li>
                    <li>
                        <a href="/{{ $locale }}/cart">@lang('web.cart') <span class="cart_count">{{ count($cart_items) }}</span></a>
                    </li>
                    <li>
                        <a class="scrollto" href="/{{ $locale }}/#contacts">@lang('web.contacts')</a>
                    </li>
                    <li>
                        <a class="scrollto" href="/{{ $locale }}/#reviews">@lang('web.reviews')</a>
                    </li>
                    <li>
                        <a class="scrollto" href="/{{ $locale }}/#about_us">@lang('web.about_us')</a>
                    </li>
                    <li>
                        <a href="/{{ $locale }}/catalog">@lang('web.catalog')</a>
                    </li>
                    <li>
                        <a href="/{{ $locale }}">@lang('web.home')</a>
                    </li>
                </ul>
            </div>
        </header>

        <div id="mobile_menu">
            <img src="/images/logo.png" />
            <a id="open_mobile_menu" href="javascript:void(0)"></a>
            @if(Request::segment(2) == 'catalog')
                <a id="open_filter_menu" href="javascript:void(0)"></a>
            @endif

            @yield('mobile_filter_block')
            <div id="mobile_menu_block">
                <a class="close-window" id="close_mobile_menu" href="javascript:void(0);">
                    <span class="fa fa-window-close"></span>
                </a>
                <ul>
                    <li>Panda Sushi</li>
                   @yield('lang')
                    <li>
                        <a href="/{{ $locale }}/">@lang('web.home')</a>
                    </li>
                    <li>
                        <a href="/{{ $locale }}/catalog">@lang('web.catalog')</a>
                    </li>
                    <li>
                        <a class="scrollto" href="/{{ $locale }}/#about_us">@lang('web.about_us')</a>
                    </li>
                    <li>
                        <a class="scrollto" href="/{{ $locale }}/#reviews">@lang('web.reviews')</a>
                    </li>
                    <li>
                        <a class="scrollto" href="/{{ $locale }}/#text_contact">@lang('web.contacts')</a>
                    </li>
                    <li>
                        <a href="/{{ $locale }}/cart">@lang('web.cart') <span class="cart_count">0</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <div id="wrapper">
            <a href="javascript:void(0)" class="scrollToTop"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
            @yield('content')
        </div>
        <footer>
            <div id="footer-content">
                <a id="main" href="/">Panda sushi</a>
                <p>
                    <span id="copy">
                        <span>©</span>  2017
                    </span>
                </p>
                <div id="share">
                    <a href="https://www.facebook.com/PandaSushi.md/"><i class="fa fa-facebook-square"></i></a>
                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </div>
            </div>
        </footer>
    </body>
</html>
@yield('scripts')
<script>
    $(document).ready(function(){

        //Check to see if the window is top if not then display button
        $(window).scroll(function(){
            if (($(this).scrollTop() > 100) && ($(this).width() > 1024)) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });

        $("a.scrollto").click(function() {

            var elementClick = $(this).attr("href");
            var destination = $(elementClick.substr(4,elementClick.length - 1)).offset().top;
            jQuery("html:not(:animated),body:not(:animated)").animate({
                scrollTop: destination
            }, 800);
            if($(window).width() < 729){
                $("#mobile_menu_block").css('transform','translate(250px,0)');
            }
            return false;
        });

        $("#open_mobile_menu").click(function (){
            $("#mobile_menu_block").css('transform','translate(-250px,0)');
        });

        $("#close_mobile_menu").click(function (){
            $("#mobile_menu_block").css('transform','translate(250px,0)');
        });

    });

</script>