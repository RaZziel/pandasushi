<html>
    <head>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
</html>
<body>
<div class="row">
        <div class="col-lg-12">
            <div style="float:left;margin-right:100px;" class="col-lg-6">
                <h3>Order Nr.{{ $order->order_id }}</h3>
                <p>Client name: {{ $order->client_name }}</p>
                <p>Client address: {{ $order->client_address }}</p>
                <p>Client phone: {{ $order->client_phone }}</p>
                <p>Total price: {{ $order->order_total_price }} lei + delivery</p>
            </div>
            <div style="float:left;" class="col-lg-6">
                <h3 class="col-lg-12">Catalogs and Quantities</h3>
                @foreach($order->order_catalogs as $order_catalog)
                    <p class="col-lg-6">Catalog: {{ $order_catalog->catalog->catalog_title }}</p>
                    <p class="col-lg-6">Quantity: {{ $order_catalog->qty }}</p>
                @endforeach
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>
</body>