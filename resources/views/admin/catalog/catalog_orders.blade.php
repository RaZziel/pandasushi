@extends('admin.layouts.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div id="error-messages"></div>
            <div id="success-message"></div>
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $page_title }}
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover text-center" id="orders">
                            <thead>
                            <tr>
                                <th class="text-center">Order Nr</th>
                                <th class="text-center">Order price</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
    <!-- /. ROW  -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables/jquery.dataTables.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#orders').DataTable( {
                ajax: {
                    url:'/admin/catalog_orders_datatable',
                    type:'POST',
                    data: {'_token': '<?=csrf_token();?>'}
                },
                columns: [
                    { data: "order_id" },
                    { data: "order_total_price" },
                    { data: "actions" }
                ],
                dom: "Bfrtip",
                "bFilter": false,
                "pageLength": 10,
                "processing": true,
                "serverSide": true,
                'fnDrawCallback': function () {
                    $('.delete').click(function () {
                        if(confirm("Are you sure you wanna delete it ?")){
                            $.ajax({
                                'url': '/admin/catalog_order_detele',
                                'type': 'POST',
                                'data': {'_token': '<?=csrf_token();?>','order_id': $(this).attr('data-order-id')},
                                success: function(data) {
                                    console.log(data);
                                    var success =  '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data + '</div>';
                                    $("#error-messages").html('');
                                    $("input").parent().removeClass('has-error');
                                    $("#success-message").html(success);
                                    setTimeout(function(){
                                        location.reload();
                                    }, 1000);
                                },
                                error: function (data) {
                                    var response = data.responseJSON;
                                    $("input").parent().removeClass('has-error');
                                    for(var key in response){
                                        $("input[name='" + key + "']").parent().addClass('has-error');
                                        var error =  '<div class="alert alert-danger fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response[key] + '</div>';
                                        $("#error-messages").append(error);
                                    }
                                }
                            });
                        }
                    });

                    $("#orders tr td:nth-child(2)").each(function (i,v) {
                        v.innerHTML = v.innerHTML + " Lei";
                    });
                }
            } );


        } );
    </script>
@endsection