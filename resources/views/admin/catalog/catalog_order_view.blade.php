@extends('admin.layouts.index')
<style>
    .panel-body p{
        padding-top: 5px;
        margin-bottom:5px;
    }

    .panel-body h3{
        margin-bottom:10px;
    }
</style>
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div id="error-messages"></div>
            <div id="success-message"></div>
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $page_title }}
                </div>
                <div class="panel-body">
                    <div class="col-lg-6">
                        <h3>Order Nr.{{ $order->order_id }}</h3>
                        <p>Client name: {{ $order->client_name }}</p>
                        <p>Client address: {{ $order->client_address }}</p>
                        <p>Client phone: {{ $order->client_phone }}</p>
                        <p>Total price: {{ $order->order_total_price }} lei + delivery</p>
                    </div>
                    <div class="col-lg-6">
                        <h3 class="col-lg-12">Catalogs and Quantities</h3>
                        @foreach($order->order_catalogs as $order_catalog)
                            <p class="col-lg-6">Catalog: {{ $order_catalog->catalog->catalog_title }}</p>
                            <p class="col-lg-6">Quantity: {{ $order_catalog->qty }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
    <!-- /. ROW  -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.form.min.js') }}"></script>
    <script>
        // wait for the DOM to be loaded
        $(document).ready(function() {

            $('#catalogCategoryForm').ajaxForm({
                success: function(data) {
                    console.log(data);
                    var success =  '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data + '</div>';
                    $("#error-messages").html('');
                    $("input").parent().removeClass('has-error');
                    $("#success-message").html(success);
                    setTimeout(function(){
                        location.href = '/admin/catalog_categories';
                    }, 1000);
                },
                error: function (data) {
                    var response = data.responseJSON;
                    $("input").parent().removeClass('has-error');
                    for(var key in response){
                        $("input[name='" + key + "']").parent().addClass('has-error');
                        var error =  '<div class="alert alert-danger fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response[key] + '</div>';
                        $("#error-messages").append(error);
                    }
                }});
        });
    </script>
@endsection