@extends('admin.layouts.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div id="error-messages"></div>
            <div id="success-message"></div>
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $page_title }}
                    <a href="/admin/catalog_category/add" class="btn btn-success right-button">Add category</a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover text-center" id="catalog">
                            <thead>
                            <tr>
                                <th class="text-center">Category name</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
    <!-- /. ROW  -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables/jquery.dataTables.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#catalog').DataTable( {
                ajax: {
                    url:'/admin/catalog_categories_datatable',
                    type:'POST',
                    data: {'_token': '<?=csrf_token();?>'}
                },
                columns: [
                    { data: "category_name_en" },
                    { data: "actions" }
                ],
                dom: "Bfrtip",
                "bFilter": false,
                "pageLength": 10,
                "processing": true,
                "serverSide": true,
                'fnDrawCallback': function () {
                    $('.delete').click(function () {
                        if(confirm("Are you sure you wanna delete it ?")){
                            $.ajax({
                                'url': '/admin/catalog_category_detele',
                                'type': 'POST',
                                'data': {'_token': '<?=csrf_token();?>','category_id': $(this).attr('data-category-id')},
                                success: function(data) {
                                    console.log(data);
                                    var success =  '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data + '</div>';
                                    $("#error-messages").html('');
                                    $("input").parent().removeClass('has-error');
                                    $("#success-message").html(success);
                                    setTimeout(function(){
                                        location.reload();
                                    }, 1000);
                                },
                                error: function (data) {
                                    var response = data.responseJSON;
                                    $("input").parent().removeClass('has-error');
                                    for(var key in response){
                                        $("input[name='" + key + "']").parent().addClass('has-error');
                                        var error =  '<div class="alert alert-danger fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response[key] + '</div>';
                                        $("#error-messages").append(error);
                                    }
                                }
                            });
                        }
                    });
                }
            } );


        } );
    </script>
@endsection