@extends('admin.layouts.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div id="error-messages"></div>
            <div id="success-message"></div>
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $page_title }}
                </div>
                <div class="panel-body">
                    <div class="col-lg-12">
                        <form id="catalogCategoryForm" action="/admin/catalog_category_update/{{ $catalog_category->category_id }}" method="post">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Categeory name romanian</label>
                                    <input name="category_name_ro" value="{{ $catalog_category->category_name_ro }}" type="text" class="form-control" placeholder="Catalog category name russian">
                                </div>
                                <div class="form-group">
                                    <label>Categeory name russian</label>
                                    <input name="category_name_ru" value="{{ $catalog_category->category_name_ru }}" type="text" class="form-control" placeholder="Catalog category name romanian">
                                </div>
                                <div class="form-group">
                                    <label>Categeory name english</label>
                                    <input name="category_name_en" value="{{ $catalog_category->category_name_en }}" type="text" class="form-control" placeholder="Catalog category name english">
                                </div>
                                <div class="form-group input-group col-lg-12 text-center">
                                    <button type="submit" class="btn btn-primary">Edit Category</button>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
    <!-- /. ROW  -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.form.min.js') }}"></script>
    <script>
        // wait for the DOM to be loaded
        $(document).ready(function() {

            $('#catalogCategoryForm').ajaxForm({
                success: function(data) {
                    console.log(data);
                    var success =  '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data + '</div>';
                    $("#error-messages").html('');
                    $("input").parent().removeClass('has-error');
                    $("#success-message").html(success);
                    setTimeout(function(){
                        location.href = '/admin/catalog_categories';
                    }, 1000);
                },
                error: function (data) {
                    var response = data.responseJSON;
                    $("input").parent().removeClass('has-error');
                    for(var key in response){
                        $("input[name='" + key + "']").parent().addClass('has-error');
                        var error =  '<div class="alert alert-danger fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response[key] + '</div>';
                        $("#error-messages").append(error);
                    }
            }});
        });
    </script>
@endsection