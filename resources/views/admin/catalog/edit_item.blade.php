@extends('admin.layouts.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div id="error-messages"></div>
            <div id="success-message"></div>
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ $page_title }}
                </div>
                <div class="panel-body">
                    <div class="col-lg-12">
                        <form id="catalogForm" action="/admin/catalog_item_save/{{ $catalog_item->catalog_id }}" method="post" >
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Catalog title</label>
                                    <input value="{{ $catalog_item->catalog_title }}" name="catalog_title" type="text" class="form-control" placeholder="Catalog title romanian">
                                </div>
                                <div class="form-group">
                                    <label>Catalog price</label>
                                    <input name="catalog_price" value="{{ $catalog_item->catalog_price }}" type="text" class="form-control" placeholder="Catalog price">
                                </div>
                                <div class="form-group">
                                    <label>Catalog category</label>
                                    <select name="catalog_category_id" class="form-control">
                                        @foreach($catalog_categories as $catalog_category)
                                            <option @if($catalog_item->catalog_category_id == $catalog_category->category_id) selected @endif value="{{ $catalog_category->category_id }}">{{ $catalog_category->category_name_en }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Hot Price</label>
                                    <select name="hot_price_status" class="form-control">
                                        <option @if(!$catalog_item->hot_price_status) selected @endif value="0">Disabled</option>
                                        <option @if($catalog_item->hot_price_status) selected @endif value="1">Enabled</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Description romanian</label>
                                    <textarea name="catalog_description_ro" class="form-control">{{ $catalog_item->catalog_description_ro }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Description russian</label>
                                    <textarea name="catalog_description_ru" class="form-control">{{ $catalog_item->catalog_description_ru }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Upload File</label>
                                    <input id="upload_image" type="file">
                                    <input value="{{ $catalog_item->upload_image_path }}" type="hidden" id="upload_image_path" name="upload_image_path" />
                                </div>
                                <div id="upload_image_box">
                                    <img width="100%" height="100%" src="{{ $catalog_item->upload_image_path }}" />
                                </div>
                                <br/><br/>
                                <div class="form-group">
                                    <label>Description english</label>
                                    <textarea name="catalog_description_en" class="form-control">{{ $catalog_item->catalog_description_en }}</textarea>
                                </div>
                            </div>
                            <div class="form-group input-group col-lg-12 text-center submit">
                                <button type="submit" class="btn btn-primary">Update catalog Item</button>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
            <!--End Advanced Tables -->
    </div>
</div>
    <!-- /. ROW  -->
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.form.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#upload_image").change(function () {

                var data = new FormData();
                var files = event.target.files;
                var old_file = 0;

                if($("#upload_image_box").find('img').attr('src')){
                    old_file = $("#upload_image_box").find('img').attr('src').replace('/storage/','');
                }

                data.append('_token',"{{ csrf_token() }}");
                data.append('old_file',old_file);

                $.each(files, function(key, value)
                {
                    data.append(key, value);
                });


                $.ajax({
                    url: '/admin/upload_file/catalog',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function (data) {
                        $("#upload_image_box").html("<img src='" + data.src + "' width='100%' height='100%' />");
                        $("#upload_image_path").val(data.src);
                    },
                    error: function(data)
                    {
                        var response = data.responseJSON;
                        console.log(response);
                        for(var key in response){
                            var error =  '<div class="alert alert-danger fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response[key] + '</div>';
                            $("#error-messages").append(error);
                        }
                    }
                });
            });
        });
    </script>
@endsection