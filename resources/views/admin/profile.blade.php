@extends('admin.layouts.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div id="error-messages"></div>
            <div id="success-message"></div>
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    User Settings
                </div>
                <div class="panel-body">
                    <div class="col-lg-4">
                        <form id="profileForm" method="post" action="/admin/profile_save" role="form">
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-user"></span>
                                </span>
                                <input name="email" value="{{ $user->email }}" type="text" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-key"></span>
                                </span>
                                <input name="old_password" type="password" class="form-control" placeholder="Old password">
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-key"></span>
                                </span>
                                <input name="new_password" type="password" class="form-control" placeholder="New password">
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <span class="fa fa-key"></span>
                                </span>
                                <input name="new_password_confirmation" type="password" class="form-control" placeholder="Repeat new password">
                            </div>
                            <div class="form-group input-group col-lg-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
    <!-- /. ROW  -->
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.form.min.js') }}"></script>
    <script>
        // wait for the DOM to be loaded
        $(document).ready(function() {

            $('#profileForm').ajaxForm({
            success: function(data) {
                console.log(data);
                var success =  '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data + '</div>';
                $("#error-messages").html('');
                $("input").parent().removeClass('has-error');
                $("#success-message").html(success);
            },
            error: function (data) {
                var response = data.responseJSON;
                $("input").parent().removeClass('has-error');
                for(var key in response){
                    $("input[name='" + key + "']").parent().addClass('has-error');
                    var error =  '<div class="alert alert-danger fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response[key] + '</div>';
                    $("#error-messages").append(error);
                }
            }});
        });
    </script>
@endsection