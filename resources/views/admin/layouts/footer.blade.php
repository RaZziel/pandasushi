
<!-- JS Scripts-->
<!-- jQuery Js -->
<script src="{{ URL::asset('js/jquery-1.10.2.js') }}"></script>
<!-- Bootstrap Js -->
<script src="{{ URL::asset('js/bootstrap/bootstrap.min.js') }}"></script>
<!-- Metis Menu Js -->
<script src="{{ URL::asset('js/jquery.metisMenu.js') }}"></script>
<!-- Morris Chart Js -->
<script src="{{ URL::asset('js/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ URL::asset('js/morris/morris.js') }}"></script>
<!-- Custom Js -->
<script src="{{ URL::asset('js/custom-scripts.js') }}"></script>

@yield('scripts')

</body>

</html>