<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/custom-styles.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.dataTables.min.css') }}" />
    <title>Panda Sushi Admin</title>
    <style>
        .right-button{
            float:right;
            margin:-7px 5px 0 0;
        }

        #upload_image_box{
            width: 300px;
            height:290px;
            background:#ecf0f1;
            border: 1px solid #bdc3c7;
            text-align:center;
            font-weight: bold;
        }

        #upload_image_box span{
            font-size:20px;
            position:relative;
            top:120px;
        }

        #catalogForm div.submit{
            margin-top:50px;
            float:left;
        }

    </style>
</head>
<body>
