@extends('layouts.main')
@section('lang')
    <li>
        <ul class="lang">
            <li>
                <a href="/ru/cart">RU</a>
            </li>
            <li>
                <a href="/en/cart">EN</a>
            </li>
            <li>
                <a href="/ro/cart">RO</a>
            </li>
        </ul>
    </li>
@endsection
@section('content')
    <div id="wrapper-dark">
        <div id="order-info-box">
            <a class="close-window" id="close_order_info_box" href="javascript:void(0);">
                <span class="fa fa-window-close"></span>
            </a>
            <form id="order_send" action="/set/order" method="post">
                <p id="order-info-title">@lang('web.order_info')</p>
                <div class="row">
                    <label>@lang('web.name')</label>
                    <input type="text" name="name">
                </div>
                <div class="row">
                    <label>@lang('web.address')</label>
                    <input type="text" name="address">
                </div>
                <div class="row">
                    <label>@lang('web.phone')</label>
                    <input type="text" name="phone">
                </div>
                <p class="amount order">
                    <label>@lang('web.order_amount'):</label>
                    <span class="price"></span>
                    <span class="currency">@lang('web.lei')</span>
                </p>
                <p class="amount delivery">
                    <label>@lang('web.delivery_amount'):</label><br/>
                </p>
                <p class="amount">
                    <label>@lang('web.delivery_price')</label>
                </p>
                <p class="total">
                    <label>@lang('web.total'):</label>
                    <span class="price"></span>
                    <input type="hidden" name="total" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <span class="currency">@lang('web.lei')</span>
                    <span> + @lang('web.delivery')</span>
                </p>
                <input id="submit_order_send" type="submit" value="@lang('web.send')" />
                <img id="rotate" src="/images/rotate.gif" width="30px" height="30px" />
            </form>
        </div>
    </div>
    <div id="content">
        <ul class="lang">
            <li>
                <a href="/ru/cart">RU</a>|
            </li>
            <li>
                <a href="/en/cart">EN</a>|
            </li>
            <li>
                <a href="/ro/cart">RO</a>
            </li>
        </ul>

        <div class="content-block" id="shopping-cart">
           <h1>
               @lang('web.shopping_cart')
           </h1>

            @foreach($catalog as $catalog_item)
               <div data-id="{{ $catalog_item->catalog_id }}" class="cart-block">
                     <div class="block-description vertical-center">
                         <?php
                            $catalog_description = 'catalog_description_'.$locale;
                         ?>
                         <p>{{ $catalog_item->catalog_title }}</p>
                         <p>{{ $catalog_item->$catalog_description }}</p>
                     </div>
                     <div class="block-image vertical-center">
                         <img width="100%" height="100%" src="{{ $catalog_item->upload_image_path }}" />
                     </div>
                     <div class="item-price vertical-center">
                         <p>
                             <span class="price">{{ $catalog_item->catalog_price }}</span>
                             <span class="currency">L</span>
                         </p>
                     </div>
                     <p class="mobile-cart-desc">{{ $catalog_item->catalog_title }}</p>
                     <div class="add-item-block">
                        <a class="item_qty_remove" href="javascript:void(0)">-</a>
                        <p class="item_qty">{{ $cart_items[$catalog_item->catalog_id] }}</p>
                        <a class="item_qty_add" href="javascript:void(0)">+</a>
                     </div>
                     <div class="item-prices vertical-center">
                        <p>
                            <span class="price">{{ $catalog_item->catalog_price*$cart_items[$catalog_item->catalog_id] }}</span>
                            <span class="currency">L</span>
                        </p>
                         <?php $total_price += $catalog_item->catalog_price*$cart_items[$catalog_item->catalog_id]; ?>
                     </div>
                     <div class="action">
                         <a class="remove_item" href="javascript:void(0)">
                             <span class="fa fa-window-close"></span>
                         </a>
                     </div>
                </div>
            @endforeach
            @if(count($cart_items) > 0)
                <h1 id="total_price">
                    @lang('web.total'): <span class="price">{{ $total_price }}</span> L
                </h1>
                <div id="place_order">
                    <a href="javascript:void(0)">@lang('web.place_an_order')</a>
                </div>
            @else
                <h3 align="center">@lang('web.cart_is_empty')</h3>
            @endif
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.form.min.js') }}"></script>

    <script>
        $(document).ready(function () {

            var cart_count = {{ count($cart_items) }};
            var totalPrice = {{ $total_price }};

            function setCartCount() {
                $('.cart_count').each(function (i,v) {
                    $(this).html(cart_count);
                });

                if(!cart_count){
                    $("#total_price").replaceWith('<h3 align="center">@lang('web.cart_is_empty')</h3>');
                    $("#place_order").remove();
                }
            }

            function setCart(sushi_id,method,qty) {
                $.ajax({
                    url: '/set/' + method,
                    type: 'POST',
                    data: {'_token': '<?=csrf_token();?>','sushi_id': sushi_id, 'qty' : qty},
                    success: function(){},
                    error: function (data) {
                        alert('web.error_try_again');
                    }
                });
            }

            $(".remove_item").click(function () {
                var block = $(this).closest('div.cart-block');
                var items_price = block.find(".item-prices .price").html();
                totalPrice = totalPrice - items_price;

                $("#total_price span.price").html(totalPrice);
                setCart(block.attr('data-id'),'remove_from_cart');
                block.remove();
                cart_count--;
                setCartCount();
            });

            $(".item_qty_remove").click(function () {
                var qty_count = $(this).siblings('.item_qty').html();
                var block = $(this).closest('div.cart-block');

                if(qty_count > 1){
                    var item_price = parseInt(block.find('.item-price .price').html());
                    var items_prices = parseInt(block.find('.item-prices .price').html()) - item_price;
                    totalPrice = totalPrice - item_price;

                    qty_count--;
                    $(this).siblings('.item_qty').html(qty_count);
                    setCart(block.attr('data-id'),'add_to_cart',qty_count);
                    block.find('.item-prices .price').html(items_prices);
                    $("#total_price .price").html(totalPrice)
                }

            });

            $(".item_qty_add").click(function () {
                var qty_count = $(this).siblings('.item_qty').html();
                var block = $(this).closest('div.cart-block');
                var item_price = parseInt(block.find('.item-price .price').html());
                var items_prices = parseInt(block.find('.item-prices .price').html()) + item_price;
                totalPrice = totalPrice + item_price;

                qty_count++;
                $(this).siblings('.item_qty').html(qty_count);
                setCart(block.attr('data-id'),'add_to_cart',qty_count);
                block.find('.item-prices .price').html(items_prices);
                $("#total_price .price").html(totalPrice)
            });

            $("#place_order a").click(function () {
               $('#wrapper-dark').fadeIn();
               $("#order-info-box .amount.order .price").html(totalPrice);
               $("#order-info-box .total .price").html(totalPrice);
               $("#order-info-box input[name='total']").val(totalPrice);
            });

            $("#close_order_info_box").click(function () {
                $(this).closest('#wrapper-dark').fadeOut();
            });

            $("#submit_order_send").click(function () {
                $('#rotate').fadeIn();
                $(this).css('display','none');
            });

            $('#order_send').ajaxForm({
                success: function(data) {
                    $('#rotate').fadeOut();
                    $('#wrapper-dark').fadeOut();
                    alert('@lang('web.order_send_message')');
                    $("#shopping-cart").html("<h1>@lang('web.shopping_cart')</h1><h3 align='center'>@lang('web.cart_is_empty')</h3>");
                    $('.cart_count').html(0);
                    $('html, body').animate({scrollTop : 0},800);
                },
                error: function (data) {

                }
            });
        });
    </script>
@endsection
