@foreach($catalog as $catalog_item)
    <?php
        $catalog_description = 'catalog_description_'.$locale;
    ?>
    <div data-id="{{ $catalog_item->catalog_id }}" class="sushi_block">
        <img src="{{ $catalog_item->upload_image_path }}" />
        <div class="sushi_block_text">
            <span class="sushi_text">{{ $catalog_item->catalog_title }} </span>
            <span class="sushi_description">{{ $catalog_item->$catalog_description }}</span>
            <span @if($catalog_item->hot_price_status) style="color:#c45f5f" @endif class="sushi_price">
                                {{ $catalog_item->catalog_price }} lei
                            </span>
            <a href="javascript:void(0);" @if(in_array($catalog_item->catalog_id,$cart_items)) class="add_sushi in_cart" @else class="add_sushi" @endif>
                <span class="fa fa-plus-square"></span>
            </a>
            <a href="javascript:void(0);" class="checked_sushi">
                <span class="fa fa-check-square"></span>
            </a>
            <a href="javascript:void(0);" class="remove_sushi">
                <span class="fa fa-window-close"></span>
            </a>
        </div>
    </div>
@endforeach