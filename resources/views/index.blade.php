@extends('layouts.main')
@section('style')
    <style type="text/css">
        @media (max-width: 1024px) {
            .sushi_block:nth-child(n + 4){
                display:none;
            }
        }
    </style>
@endsection
@section('lang')
    <li>
        <ul class="lang">
            <li>
                <a href="/ru">RU</a>
            </li>
            <li>
                <a href="/en">EN</a>
            </li>
            <li>
                <a href="/ro">RO</a>
            </li>
        </ul>
    </li>
@endsection
@section('content')
    <div id="content">
        <ul class="lang">
            <li>
                <a href="/ru">RU</a>|
            </li>
            <li>
                <a href="/en">EN</a>|
            </li>
            <li>
                <a href="/ro">RO</a>
            </li>
        </ul>
        <div class="content-block" id="slider">
            <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:380px;overflow:hidden;visibility:hidden;">
                <!-- Loading Screen -->
                <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                    <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="/img/spin.svg" />
                </div>
                <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
                    <div>
                        <img data-u="image" src="/img/1.jpg" />
                    </div>
                    <div>
                        <img data-u="image" src="/img/2.jpg" />
                    </div>
                    <div>
                        <img data-u="image" src="/img/3.jpg" />
                    </div>
                    <div>
                        <img data-u="image" src="/img/4.jpg" />
                    </div>
                    <div>
                        <img data-u="image" src="/img/5.jpg" />
                    </div>
                    <a data-u="any" href="https://www.jssor.com" style="display:none">bootstrap slider</a>
                </div>

                <!-- Arrow Navigator -->
                <div data-u="arrowleft" class="jssora061" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <path class="a" d="M11949,1919L5964.9,7771.7c-127.9,125.5-127.9,329.1,0,454.9L11949,14079"></path>
                    </svg>
                </div>
                <div data-u="arrowright" class="jssora061" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                    <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                        <path class="a" d="M5869,1919l5984.1,5852.7c127.9,125.5,127.9,329.1,0,454.9L5869,14079"></path>
                    </svg>
                </div>
            </div>
        </div>
        <div class="content-block">
            <p id="hot_price_title">@lang('web.hot_price')</p>
            <div id="hot_price">
                <div id="hot_price_tablet">
                    @foreach($catalog as $catalog_item)
                        <?php
                            $catalog_description = 'catalog_description_'.$locale;
                        ?>
                        <div data-id="{{ $catalog_item->catalog_id }}" class="sushi_block">
                            <img src="{{ $catalog_item->upload_image_path }}" />
                            <div class="sushi_block_text">
                                <div class="sushi_block_text_tablet">
                                     <span class="sushi_text">
                                     {{ $catalog_item->catalog_title }}
                                </span>
                                <span class="sushi_description">
                                     {{ $catalog_item->$catalog_description }}
                                </span>
                                <span class="sushi_price">
                                    {{ $catalog_item->catalog_price }} @lang('web.lei')
                                </span>
                                    <a @if(in_array($catalog_item->catalog_id,$cart_items)) class="add_sushi in_cart" @else class="add_sushi" @endif href="javascript:void(0);" >
                                        <span class="fa fa-plus-square"></span>
                                    </a>
                                    <a class="checked_sushi checked"  href="javascript:void(0);" >
                                        <span class="fa fa-check-square"></span>
                                    </a>
                                    <a href="javascript:void(0);" class="remove_sushi">
                                        <span class="fa fa-window-close"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="content-block" id="catalog_see_all">
                    <a href="/{{ $locale }}/catalog" >@lang('web.see_all')</a>
                </div>
                <div class="content-block" id="about_us">
                    <div id="about-us-text">
                        <p id="about-us-title">
                            @lang('web.about_us')
                        </p>
                        <p id="about-us-subtitle">
                            @lang('web.about_us_subtitle')
                        </p>
                        <p id="about-us-content">
                            @lang('web.about_us_content')
                        </p>
                    </div>
                </div>
                <div class="content-block" id="reviews">
                    <div class="review">
                        <div class="review-image">
                            <img src="/images/boy.png" />
                        </div>
                        <div class="review-text">
                            <span class="fa fa-quote-left"></span>
                            <p>
                                @lang('web.boy_review')
                            </p>
                            <span class="fa fa-quote-right"></span>
                        </div>
                        <div class="review-name">
                            <span></span>
                            <p>@lang('web.boy_review_name')</p>
                        </div>
                    </div>
                    <div class="review">
                        <div class="review-image">
                            <img src="/images/girl.png" />
                        </div>
                        <div class="review-text">
                            <span class="fa fa-quote-left"></span>
                            <p>
                                @lang('web.girl_review')
                            </p>
                            <span class="fa fa-quote-right"></span>
                        </div>
                        <div class="review-name">
                            <span></span>
                            <p>@lang('web.girl_review_name')</p>
                        </div>
                    </div>
                </div>
                <div class="content-block" id="contacts">
                    <div class="contact-block">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Ab5f75f33819baa349429aaaf335cf7a08d8437bb0903672fb57ab35f0e25dd9b&amp;source=constructor&lang={{ config('ctr.yandexlang.'.$locale) }}" width="100%" height="412" frameborder="0"></iframe>
                    </div>
                    <div class="contact-block" id="text_contact">
                        <p>@lang('web.contact_us_address')</p>
                        <p>+373 60 760 750</p>
                        <p>@lang('web.working_hours'):</p>
                        <p>@lang('web.mn_fr') 10:00 - 22:00</p>
                        <p>@lang('web.sa_su') 10:00 - 23:00</p>
                        <a href="https://www.facebook.com/PandaSushi.md/">@lang('web.contact_us')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jssor.slider-26.1.5.min.js') }}"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            var jssor_1_SlideshowTransitions = [
                {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
                $AutoPlay: 1,
                $SlideshowOptions: {
                    $Class: $JssorSlideshowRunner$,
                    $Transitions: jssor_1_SlideshowTransitions,
                    $TransitionsOrder: 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,
                    $Cols: 1,
                    $Orientation: 2,
                    $Align: 0,
                    $NoDrag: true
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = false;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
            var cart_count = {{ count($cart_items) }};

            function setCartCount() {
                $('.cart_count').each(function (i,v) {
                    $(this).html(cart_count);
                });
            }

            function setCart(sushi_id,method) {
                $.ajax({
                    url: '/set/' + method,
                    type: 'POST',
                    data: {'_token': '<?=csrf_token();?>','sushi_id': sushi_id},
                    success: function(){},
                    error: function (data) {
                        alert('Error.Try again!');
                    }
                });
            }

            function check_item(object) {
                $(object).css('display','none');
                $(object).siblings('a.checked_sushi').css('display','block').one('mouseout',function () {
                    $(this).mouseenter(function () {
                        $(this).siblings('.remove_sushi').css('display','block');
                        $(this).css('display','none');
                    });

                    $('.sushi_block .remove_sushi').mouseleave(function () {
                        if($(this).siblings('a.add_sushi').css('display') == 'none'){
                            $(this).siblings('a.checked_sushi').css('display','block');
                            $(this).css('display','none');
                        }
                    });
                });
            }

            $(".sushi_block .add_sushi").click(function () {
                var sushi_id = $(this).closest('div.sushi_block').attr('data-id');
                check_item(this);
                setCart(sushi_id,'add_to_cart');
                cart_count++;
                setCartCount();
            });

            $(".sushi_block .in_cart").each(function (i,v) {
                check_item(v);
                var checked_sushi = $(v).siblings('a.checked_sushi');

                $(checked_sushi).mouseenter(function () {
                    $(this).siblings('.remove_sushi').css('display','block');
                    $(this).css('display','none');
                });

            });

            $(".sushi_block .remove_sushi").click(function () {
                var sushi_id = $(this).closest('div.sushi_block').attr('data-id');

                $(this).parent().find('a').css('display','none').unbind('mouseenter');
                $(this).siblings('.add_sushi').css('display','block');
                cart_count--;
                setCartCount();

                setCart(sushi_id,'remove_from_cart');
            });
        });


    </script>

@endsection
