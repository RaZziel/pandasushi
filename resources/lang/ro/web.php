<?php

return [
    'home'                      => 'Acasă',
    'catalog'                   => 'Catalog',
    'about_us'                  => 'Despre noi',
    'reviews'                   => 'Opinii',
    'contacts'                  => 'Contacte',
    'cart'                      => 'Cos',
    'name'                      => 'Nume',
    'address'                   => 'Adresa',
    'phone'                     => 'Telefon',
    'send'                      => 'Trimite',
    'error_try_again'           => 'Eroare.Încearcă inca o data!',
    'order_send_message'        => 'Comanda a fost făcută.',

    // Index
    'hot_price'                 => 'Super preț',
    'see_all'                   => 'Vezi toate',

    //currency
    'lei'                       => 'lei',

    //catalog
    'product_categories'        => 'Categorii de produse',
    'products'                  => 'Produse',
    'see_more'                  => 'Vezi mai mult',
    'catalog_is_empty'          => 'Catalogul este gol',

    //cart
    'order_info'                => 'Informație despre comandă',
    'order_amount'              => 'Prețul comenzii',
    'delivery_amount'           => 'Prețul de livrare',
    'total'                     => 'Total',
    'delivery'                  => 'Livrare',
    'shopping_cart'             => 'Cosul meu',
    'place_an_order'            => 'Comanda',
    'cart_is_empty'             => 'Cosul este gol',
    'delivery_price'            => 'Riscani(10 lei),Alte(35 lei)',

    //contact_us
    'contact_us_address'        => 'Chishinau, Bulevardul Moscovei 20',
    'working_hours'             => 'Ore de lucru',
    'mn_fr'                     => 'Lu-Vn',
    'sa_su'                     => 'Sa-Du',
    'contact_us'                => 'Contacteaza-ne',

    //reviews
    'girl_review_name'          => 'Alina Florescu',
    'boy_review_name'           => 'Vadim Besliu',
    'girl_review'               => 'Cele mai bune sushi din oras! Livrare foarte rapida! Cu suguranta o sa le comand din nou. Recomand tutorora.',
    'boy_review'                => 'Sincer sã fiu,comand foarte rar. Dar am ramas multumit de calitatea celor de la Panda Sushi.Recomand tutorora.',

    //header text
    'header_text_title'         => 'Tradițiile japoneze pentru cei care apreciază calitatea.',
    'header_text'               => 'Panda Sushi - este o adevarata echipa de profesionisti si maestri ai mestesugurilor lor. Activitatea noastră combină experiența bogată, cunoașterea profundă a bucătăriei japoneze, precum și toate detaliile gătitului celor mai deliciosi și gustosi sushi pentru dumnevoastra. De aceea clienții care au comandat de la noi o dată - devin clienți obișnuiți!',

    //about us
    'about_us_subtitle'         => 'Panda Sushi este livrarea de mâncăruri japoneze la Chișinău. Echipa noastră constă din profesioniști,care își iubesc lucru.',
    'about_us_content'          => 'Sushi este baza culturii alimentare japoneze. Panda Sushi are întotdeauna produse proaspete, mereu delicioase și, cel mai important, calitative. Întreaga gamă de feluri de mâncare constă în sushi japonez original, pregătit cu toate subtilitățile și regulile. Până în prezent livrăm: Seturi, Rolls, Sushi, Gunkan, Maki și Motiko. Misiunea noastră este de a face pe clienții noștri fericiți și bine hrăniți. Indiferent ce ne costă, vom livra o dispozitie buna pentru fiecare. Cele mai delicioase sushi - încercați-le chiar acum!',
];