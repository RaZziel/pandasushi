<?php

return [
    'home'                      => 'Home',
    'catalog'                   => 'Catalog',
    'about_us'                  => 'About us',
    'reviews'                   => 'Reviews',
    'contacts'                  => 'Contacts',
    'cart'                      => 'Cart',
    'name'                      => 'Name',
    'address'                   => 'Address',
    'phone'                     => 'Phone',
    'send'                      => 'Send',
    'error_try_again'           => 'Error.Try again!',
    'order_send_message'        => 'Your order has been send.',

    // Index
    'hot_price'                 => 'Hot price',
    'see_all'                   => 'See all',

    //currency
    'lei'                       => 'lei',

    //catalog
    'product_categories'        => 'Product categories',
    'products'                  => 'Products',
    'see_more'                  => 'See more',
    'catalog_is_empty'          => 'Catalog is empty',

    //cart
    'order_info'                => 'Order info',
    'order_amount'              => 'Order amount',
    'delivery_amount'           => 'Delivery amount',
    'total'                     => 'Total',
    'delivery'                  => 'Delivery',
    'shopping_cart'             => 'Shopping cart',
    'place_an_order'            => 'Place an order',
    'cart_is_empty'             => 'Cart is empty',
    'delivery_price'            => 'Riscanovka(10 lei),Other(35 lei)',

    //contact_us
    'contact_us_address'        => 'Chishinau, Bulevardul Moscovei 20',
    'working_hours'             => 'Working hours',
    'mn_fr'                     => 'Mn-Fr',
    'sa_su'                     => 'Sa-Su',
    'contact_us'                => 'Contact us',

    //reviews
    'girl_review_name'          => 'Alina Florescu',
    'boy_review_name'           => 'Sergiu Bellu',
    'girl_review'               => 'No doubt - the best sushi in the city! Very fast delivery! Surely will order these sushi again.',
    'boy_review'                => 'Honestly, I rarely order sushi. But I was satisfied with the quality of Panda Sushi. I recommend it to everyone.',

    //header text
    'header_text_title'         => 'Japanese traditions for those who appreciate quality.',
    'header_text'               => 'Panda Sushi - it\'s a real team of professionals and masters of their craft. Our work combines rich experience, deep knowledge of Japanese cuisine, as well as all the details of cooking the most delicious and tasty sushi for you. That is why customers who have ordered from us once - become regular customers!',

    //about us
    'about_us_subtitle'         => 'Panda Sushi is the delivery of Japanese cuisine in Chisinau. Our team are real professionals who love their work.',
    'about_us_content'          => 'Sushi is the basis of the Japanese food culture. Panda Sushi always has fresh products, always delicious, and always qualitatively. The whole range of our dishes consists in original Japanese sushi, prepared with all the subtleties and rules. To date, we deliver: Sets, Rolls, Sushi, Gunkan, Maki and Motiko. Our job is to make our customers happy and well fed. Whatever it cost us, we will deliver happiness to every place. Best and most delicious sushi - try it right now!',
];