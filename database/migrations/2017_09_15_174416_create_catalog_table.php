<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog', function (Blueprint $table) {
            $table->increments('catalog_id');
            $table->string('catalog_title_ro');
            $table->string('catalog_title_ru');
            $table->string('catalog_title_en');
            $table->string('catalog_description_ro');
            $table->string('catalog_description_ru');
            $table->string('catalog_description_en');
            $table->string('catalog_price');
            $table->UnsignedInteger('catalog_category_id');
            $table->tinyInteger('hot_price_status');
            $table->string('upload_image_path');
            $table->foreign('catalog_category_id')
                ->references('category_id')
                ->on('catalog_categories')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog');
    }
}
