<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_catalog', function (Blueprint $table) {
            $table->UnsignedInteger('order_id');
            $table->UnsignedInteger('catalog_id');
            $table->integer('qty');
            $table->foreign('catalog_id')
                ->references('catalog_id')
                ->on('catalog')
                ->onDelete('cascade');
            $table->foreign('order_id')
                ->references('order_id')
                ->on('orders')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_catalog');
    }
}
