/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100125
Source Host           : localhost:3306
Source Database       : pandasushi

Target Server Type    : MYSQL
Target Server Version : 100125
File Encoding         : 65001

Date: 2017-06-01 08:45:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for catalog
-- ----------------------------
DROP TABLE IF EXISTS `catalog`;
CREATE TABLE `catalog` (
  `catalog_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catalog_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catalog_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catalog_category_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `hot_price_status` tinyint(4) NOT NULL,
  `upload_image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`catalog_id`),
  KEY `catalog_language_id_foreign` (`language_id`),
  KEY `catalog_catalog_category_id_foreign` (`catalog_category_id`),
  CONSTRAINT `catalog_catalog_category_id_foreign` FOREIGN KEY (`catalog_category_id`) REFERENCES `catalog_categories` (`category_id`) ON DELETE CASCADE,
  CONSTRAINT `catalog_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of catalog
-- ----------------------------
INSERT INTO `catalog` VALUES ('2', 'Philadelphia unu', '250', '2', '1', '1', '/storage/catalog/aIkEh2xmoQSGy6ZKPPg9iLbpPyaBtndsQH7IrLid.png', '2017-10-19 13:04:52', '2017-10-19 13:04:52');
INSERT INTO `catalog` VALUES ('3', 'Philadelphia doi', '260', '2', '1', '1', '/storage/catalog/2DYLakWtgsQZkuRFGwxGgbfSTd6p2wurjT5oWmTw.png', '2017-10-19 13:05:08', '2017-10-19 13:12:15');
INSERT INTO `catalog` VALUES ('4', 'Philadelphia trei', '270', '2', '1', '1', '/storage/catalog/nt3baz5WpyaA02x4xyKNsKJowADRPyCkaxAZloHw.png', '2017-10-19 13:05:24', '2017-10-19 13:05:24');
INSERT INTO `catalog` VALUES ('5', 'Philadelphia pantru', '200', '2', '1', '1', '/storage/catalog/S9ywFFsjTo2503n4Sdi1VNB3FaXVl1ZjghqKDe30.png', '2017-10-19 13:09:49', '2017-10-19 13:09:49');
INSERT INTO `catalog` VALUES ('6', 'Philadelphia cinci', '600', '2', '1', '1', '/storage/catalog/3UfcztUwlkwytvkVehtOXsdWVpcQiTe4p3DQyT0w.png', '2017-10-19 13:12:31', '2017-10-19 13:12:31');
INSERT INTO `catalog` VALUES ('7', 'Philadelphia sase', '450', '2', '1', '1', '/storage/catalog/Icom8XOyISbZrgAEQgU38vQKIgN2efLXyZKjm6jo.png', '2017-10-19 13:13:01', '2017-10-19 13:13:01');

-- ----------------------------
-- Table structure for catalog_categories
-- ----------------------------
DROP TABLE IF EXISTS `catalog_categories`;
CREATE TABLE `catalog_categories` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `catalog_categories_language_id_foreign` (`language_id`),
  CONSTRAINT `catalog_categories_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of catalog_categories
-- ----------------------------
INSERT INTO `catalog_categories` VALUES ('2', 'Sushi', '1', '2017-10-19 13:02:06', '2017-10-19 13:02:58');
INSERT INTO `catalog_categories` VALUES ('3', 'Rulouri', '1', '2017-10-19 13:02:49', '2017-10-19 13:03:05');

-- ----------------------------
-- Table structure for contracts
-- ----------------------------
DROP TABLE IF EXISTS `contracts`;
CREATE TABLE `contracts` (
  `adress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  KEY `contracts_language_id_foreign` (`language_id`),
  CONSTRAINT `contracts_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of contracts
-- ----------------------------

-- ----------------------------
-- Table structure for information
-- ----------------------------
DROP TABLE IF EXISTS `information`;
CREATE TABLE `information` (
  `information_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `information_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `information_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`information_id`),
  KEY `information_language_id_foreign` (`language_id`),
  CONSTRAINT `information_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of information
-- ----------------------------

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `language_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('1', 'ro');
INSERT INTO `languages` VALUES ('2', 'ru');
INSERT INTO `languages` VALUES ('3', 'en');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2014_10_12_110000_create_languages_table', '1');
INSERT INTO `migrations` VALUES ('4', '2017_09_15_173638_create_catalog_categories_table', '1');
INSERT INTO `migrations` VALUES ('5', '2017_09_15_174416_create_catalog_table', '1');
INSERT INTO `migrations` VALUES ('6', '2017_09_15_181128_create_information_table', '1');
INSERT INTO `migrations` VALUES ('7', '2017_09_15_182041_create_orders_table', '1');
INSERT INTO `migrations` VALUES ('8', '2017_09_15_182821_create_orders_catalog_table', '1');
INSERT INTO `migrations` VALUES ('9', '2017_09_15_183203_create_contacts_table', '1');
INSERT INTO `migrations` VALUES ('10', '2017_09_16_055002_create_reviews_table', '1');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_total_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '[{\"catalog_id\":1,\"qty\":4},{\"catalog_id\":2,\"qty\":7},{\"catalog_id\":5,\"qty\":9}]', '100', 'Roman', 'Brisbane', '0788885', '2017-06-01 07:00:55', '2017-06-08 07:00:58');

-- ----------------------------
-- Table structure for orders_catalog
-- ----------------------------
DROP TABLE IF EXISTS `orders_catalog`;
CREATE TABLE `orders_catalog` (
  `order_id` int(10) unsigned NOT NULL,
  `catalog_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `orders_catalog_catalog_id_foreign` (`catalog_id`),
  KEY `orders_catalog_order_id_foreign` (`order_id`),
  CONSTRAINT `orders_catalog_catalog_id_foreign` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`catalog_id`),
  CONSTRAINT `orders_catalog_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of orders_catalog
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for reviews
-- ----------------------------
DROP TABLE IF EXISTS `reviews`;
CREATE TABLE `reviews` (
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review_text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `reviews_language_id_foreign` (`language_id`),
  CONSTRAINT `reviews_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of reviews
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Pavlik	', 'razziel93@gmail.com', '$2y$10$.z1NNp/P0BNhxRgS0zMpqu9qNF4V7scMU61iAsLlMshD0xRvdwHLS', 'gGWLAJXQgqXlCe5BdRutvBaUvOMZC7geDOTnhJejBnxY6yywSICrw7Bgohx9', '2017-06-01 05:38:00', '2017-06-01 05:38:03');
SET FOREIGN_KEY_CHECKS=1;
