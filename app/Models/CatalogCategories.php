<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatalogCategories extends Model
{
    protected $primaryKey = 'category_id';

    protected $fillable = [
        'category_id',
        'category_name_ro',
        'category_name_ru',
        'category_name_en',
    ];
}
