<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $primaryKey = 'order_id';

    protected $fillable = [
        'order_details',
        'order_total_price',
        'client_name',
        'client_address',
        'client_phone',
    ];

    public function order_catalogs(){

        return $this->hasMany('App\Models\OrdersCatalog','order_id','order_id');
    }
}
