<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $primaryKey = 'catalog_id';

    protected $table = "catalog";

    protected $fillable = [
        'catalog_title',
        'catalog_description_ro',
        'catalog_description_ru',
        'catalog_description_en',
        'catalog_price',
        'catalog_category_id',
        'hot_price_status',
        'upload_image_path',
    ];
}
