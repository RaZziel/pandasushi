<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdersCatalog extends Model
{
    protected $table = "orders_catalog";

    protected $fillable = [
        'order_id',
        'catalog_id',
        'qty'
    ];

    public function catalog(){
        return $this->belongsTo('App\Models\Catalog','catalog_id','catalog_id');
    }
}
