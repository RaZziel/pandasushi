<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\CatalogCategories;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Testing\Fakes\MailFake;

class CatalogController extends Controller
{
    public function index(Request $request){

        $catalog = Catalog::limit(config('ctr.common.limit'))->orderBy('catalog_id', 'desc')->get();

        $catalog_categories = CatalogCategories::all();

        $cart_items = $request->session()->get('cart') ? array_keys($request->session()->get('cart')) : [];

        $locale = App::getLocale();

        return view('catalog', ['catalog' => $catalog, 'catalog_categories' => $catalog_categories, 'locale' => $locale, 'cart_items' => $cart_items]);
    }

    public function cart(Request $request){

        $locale = App::getLocale();
        $cart_items = $request->session()->get('cart');
        $cart_items_keys = $cart_items ? array_keys($cart_items) : [];
        $catalog = Catalog::whereIn('catalog_id',$cart_items_keys)->get();
        $total_price = 0;

        return view('cart',['locale' => $locale, 'catalog' => $catalog, 'cart_items' => $cart_items,'total_price' => $total_price]);
    }

    public function get_catalog(Request $request){

        $catalog_categories = $request->get('catalog_categories');
        $cart_items = $request->session()->get('cart') ? array_keys($request->session()->get('cart')) : [];
        $limit = $request->get('limit');

        $catalog = Catalog::where(function($query) use($catalog_categories){
                if(is_array($catalog_categories)){
                    $query->whereIn('catalog_category_id',$catalog_categories);
                }
            })
            ->limit($limit)
            ->orderBy('catalog_id', 'desc')
            ->get();

        $locale = App::getLocale();

        return view('ajax.get_catalog',['catalog' => $catalog, 'locale' => $locale, 'cart_items' => $cart_items]);
    }

    public function order_send(Request $request){
        $cart_items = $request->session()->get('cart') ? $request->session()->get('cart') : [];
        $input = $request->all();

        DB::transaction(function () use($input,$cart_items) {

            $order = Orders::create([
                'client_name'           => $input['name'],
                'client_address'        => $input['address'],
                'client_phone'          => $input['phone'],
                'order_total_price'     => $input['total']
            ]);

            foreach ($cart_items as $catalog_id => $qty){

                $order->order_catalogs()->create([
                    'qty'           => $qty,
                    'catalog_id'   => $catalog_id,
                ]);
            }

            Mail::send('email.order_details', ['order' => $order], function ($message) use($order) {
                $message->from('admin@admin.com', 'Pandasushi Order Nr.'.$order->order_id);
                $message->subject('Order Nr.'.$order->order_id);
                $message->to('pandasushimd@gmail.com');
            });

        });

        $request->session()->forget('cart');
        return response()->json('true');
    }

    public function add_to_cart(Request $request){

        if($request->get('sushi_id')){
            $cart = $request->session()->get('cart') ? $request->session()->get('cart') : [];
            $cart[$request->get('sushi_id')] = $request->get('qty') ? $request->get('qty') : 1;
            $request->session()->put('cart',$cart);
        }
    }

    public function remove_from_cart(Request $request){

        if($request->get('sushi_id')){
            $cart = $request->session()->get('cart') ? $request->session()->get('cart') : [];
            unset($cart[$request->get('sushi_id')]);
            $request->session()->put('cart',$cart);
        }
    }
  
}
