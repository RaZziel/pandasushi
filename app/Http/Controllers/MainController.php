<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MainController extends Controller
{
    public function index(Request $request){
        $catalog = Catalog::where(['hot_price_status' => 1])
            ->limit(6)
            ->orderBy('catalog_id', 'desc')
            ->get();

        $locale = App::getLocale();
        $cart_items = $request->session()->get('cart') ? array_keys($request->session()->get('cart')) : [];

        return view('index',['catalog' => $catalog, 'locale' => $locale, 'cart_items' => $cart_items]);
    }
}
