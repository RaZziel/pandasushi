<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function user_profile(){
        $user = Auth::user();
        $page_title = "User Settings";

        return view('admin.profile', array('user' => $user, 'page_title' => $page_title,'active' => ''));
    }

    public function profile_save(Request $request){

        $inputs = $request->all();

        $rules = array(
            'email'                     => 'required',
            'old_password'              => 'required|old_password:' . Auth::user()->password,
            'new_password'              => 'required|min:6|confirmed',
            'new_password_confirmation' => 'required|min:6|same:new_password',
        );

        $message = array(
            'old_password'  => 'The password is not right'
        );

        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {

            return Hash::check($value, current($parameters));

        });


        $this->validate($request,$rules,$message);

        $user = Auth::user();
        $user->email = $inputs['email'];
        $user->password = Hash::make($inputs['new_password']);
        $user->save();

        return response()->json('Profile settings were changed');

    }
}
