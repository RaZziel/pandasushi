<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Catalog;
use App\Models\CatalogCategories;
use App\Models\Languages;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CatalogController extends Controller
{
    public function catalog_categories(){

        $page_title = "Catalog Categories Table";
        $active = "catalog_categories";

        return view('admin.catalog.catalog_categories', array('page_title' => $page_title, 'active' => $active));
    }

    public function catalog_categories_datatable(Request $request){

        $catalog_categories['data'] = CatalogCategories::skip($request->start)
            ->take($request->length)
            ->orderBy('category_id', 'desc')
            ->get();

        $catalog_categories_count = CatalogCategories::all()->count();
        $catalog_categories['recordsTotal'] = $catalog_categories_count;
        $catalog_categories['recordsFiltered'] = $catalog_categories_count;


        foreach ($catalog_categories['data'] as $catalog_category){
            $catalog_category['actions'] = "<a href='/admin/catalog_category/edit/".$catalog_category->category_id."' style='margin-right:5px;' class=\"btn btn-primary\"><i class=\"fa fa-edit edit\"></i></a><button data-category-id=\"".$catalog_category->category_id."\" class=\"btn btn-danger delete\"><i class=\"fa fa-trash-o\"></i></button>";
        }
        return response()->json($catalog_categories);
    }

    public function catalog_category_add(){

        $page_title = "Catalog Category";
        $active = "catalog_categories";

        return view('admin.catalog.catalog_category_add', array('page_title' => $page_title, 'active' => $active));
    }

    public function catalog_category_edit($id){

        $catalog_category = CatalogCategories::findOrFail($id);

        $page_title = "Edit catalog Category";
        $active = "catalog_categories";

        return view('admin.catalog.catalog_category_edit', array('page_title' => $page_title, 'active' => $active, 'catalog_category' => $catalog_category));
    }

    public function catalog_category_save(Request $request){

        $inputs = $request->all();

        $rules = array(
            'category_name_ro'     => 'string|required',
            'category_name_ru'     => 'string|required',
            'category_name_en'     => 'string|required',
        );

        $this->validate($request,$rules);

        CatalogCategories::create($inputs);

        return response()->json('Catalog category was added');

    }

    public function catalog_category_update($id,Request $request){

        $catalog_category = CatalogCategories::findOrFail($id);

        $inputs = $request->all();

        $rules = array(
            'category_name_ro'     => 'string|required',
            'category_name_ru'     => 'string|required',
            'category_name_en'     => 'string|required',

        );

        $this->validate($request,$rules);

        $catalog_category->fill($inputs);
        $catalog_category->save();

        return response()->json('Catalog category was edited');

    }

    public function catalog_category_detele(Request $request){

        CatalogCategories::where('category_id',$request->category_id)->firstOrFail()->delete();

        return response()->json('Catalog category was successfully deleted');
    }

    public function catalog_index(){

        $page_title = "Catalog Table";
        $active = "catalog";

        return view('admin.catalog.index', array('page_title' => $page_title, 'active' => $active));
    }

    public function catalog_datatable(Request $request){

        $catalog['data'] = Catalog::join('catalog_categories','catalog.catalog_category_id','=','catalog_categories.category_id')
            ->skip($request->start)
            ->take($request->length)
            ->orderBy('catalog_id', 'desc')
            ->get();

        $catalog_count = Catalog::all()->count();
        $catalog['recordsTotal'] = $catalog_count;
        $catalog['recordsFiltered'] = $catalog_count;


        foreach ($catalog['data'] as $catalog_item){
            $catalog_item['actions'] = "<a href='/admin/catalog/edit_item/".$catalog_item->catalog_id."' style='margin-right:5px;' class=\"btn btn-primary\"><i class=\"fa fa-edit edit\"></i></a><button data-catalog-id=\"".$catalog_item->catalog_id."\" class=\"btn btn-danger delete\"><i class=\"fa fa-trash-o\"></i></button>";
            $catalog_item['price_status']   = $catalog_item->hot_price_status ? "Enabled" : "Disabled";
        }
        return response()->json($catalog);
    }

    public function add_catalog_item(){

        $page_title = "Add catalog item";
        $active = "catalog";
        $catalog_categories = CatalogCategories::all();

        return view('admin.catalog.add_item',array('page_title' => $page_title, 'active' => $active, 'catalog_categories' => $catalog_categories));
    }

    public function edit_catalog_item($id){

        $page_title = "Edit catalog item";
        $active = "catalog";
        $catalog_categories = CatalogCategories::all();
        $catalog_item = Catalog::findOrFail($id);

        return view('admin.catalog.edit_item',array('page_title' => $page_title, 'active' => $active, 'catalog_categories' => $catalog_categories, 'catalog_item'  => $catalog_item));
    }

    public function catalog_item_save(Request $request,$id = false){

        $validator = Validator::make($request->all(),[
            'catalog_title'                      => 'required|string',
            'catalog_description_ro'             => 'required|string',
            'catalog_description_ru'             => 'required|string',
            'catalog_description_en'             => 'required|string',
            'catalog_price'                      => 'required|integer',
            'catalog_category_id'                => 'required|integer',
            'hot_price_status'                   => 'required|integer',
            'upload_image_path'                  => 'required|string'
        ],[
            'upload_image_path.required'         => 'Image is required',
            'catalog_price.integer'              => 'Price should be a number',
            'catalog_title.required'             => 'Catalog title is required',
            'catalog_description_ro.required'    => 'Romanian catalog description is required',
            'catalog_description_ru.required'    => 'Russian catalog description is required',
            'catalog_description_en.required'    => 'English catalog description is required',
            'catalog_price.required'             => 'Catalog price is required',
        ]);

        if($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        if($id){
            $catalog = Catalog::findOrFail($id);
            $catalog->fill($request->all());
            $catalog->save();
        }else{
            Catalog::create($request->all());
        }


        return redirect('/admin/catalog');
    }

    public function catalog_detele(Request $request){

        Catalog::where('catalog_id',$request->catalog_id)->firstOrFail()->delete();

        return response()->json('Catalog was successfully deleted');
    }

    public function orders_index(){

        $page_title = "Order Table";
        $active = "orders";

        return view('admin.catalog.catalog_orders', array('page_title' => $page_title, 'active' => $active));
    }

    public function catalog_orders_datatable(Request $request){

        $orders['data'] = Orders::skip($request->start)
            ->take($request->length)
            ->orderBy('order_id', 'desc')
            ->get();

        $orders_count = Orders::all()->count();
        $orders['recordsTotal'] = $orders_count;
        $orders['recordsFiltered'] = $orders_count;

        foreach ($orders['data'] as $order){
            $order['actions'] = "<a href='/admin/orders/view_order/".$order->order_id."' style='margin-right:5px;' class=\"btn btn-primary\"><i class=\"fa fa-eye\"></i></a><button data-order-id=\"".$order->order_id."\" class=\"btn btn-danger delete\"><i class=\"fa fa-trash-o\"></i></button>";
        }
        return response()->json($orders);
    }

    public function catalog_order_view($id){

        $page_title = "Order details";
        $active = "orders";
        $order = Orders::findOrFail($id);

        return view('admin.catalog.catalog_order_view', array('page_title' => $page_title, 'active' => $active,'order' => $order));
    }

    public function catalog_order_detele(Request $request){

        Orders::where('order_id',$request->order_id)->firstOrFail()->delete();

        return response()->json('Order was successfully deleted');
    }

}
