<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FileController extends Controller
{
    public function upload_file(Request $request,$directory_map){

        $this->validate($request,[
            0 =>  'required | mimes:jpeg,jpg,png',
        ],[
            0 => 'Invalid type of file'
        ]);

        $file_path = $request->file(0)->store($directory_map,'public');

        if($request->input('old_file')){
            File::delete(storage_path('/app/public/'.$request->input('old_file')));
        }

        return response()->json(['src' => '/storage/'.$file_path]);
    }
}
