<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/','MainController@index');
    Route::get('/catalog','CatalogController@index');
    Route::get('/cart','CatalogController@cart');
    Route::get('/txdheap6qa','CatalogController@test');

    Route::post('/get/get_catalog','CatalogController@get_catalog');
    Route::post('/set/order','CatalogController@order_send');
    Route::post('/set/add_to_cart','CatalogController@add_to_cart');
    Route::post('/set/remove_from_cart','CatalogController@remove_from_cart');

});


Route::group(['middleware' => ['auth'],'namespace' => 'Admin' ], function () {

    Route::get('/admin/catalog', 'CatalogController@catalog_index');
    Route::get('/admin/catalog/add_item', 'CatalogController@add_catalog_item');
    Route::get('/admin/catalog/edit_item/{id}', 'CatalogController@edit_catalog_item');
    Route::post('/admin/catalog_datatable', 'CatalogController@catalog_datatable');
    Route::post('/admin/catalog_detele', 'CatalogController@catalog_detele');
    Route::get('/admin/catalog_categories', 'CatalogController@catalog_categories');
    Route::post('/admin/catalog_categories_datatable', 'CatalogController@catalog_categories_datatable');
    Route::post('/admin/catalog_category_detele', 'CatalogController@catalog_category_detele');
    Route::get('/admin/catalog_category/add', 'CatalogController@catalog_category_add');
    Route::get('/admin/catalog_category/edit/{id}', 'CatalogController@catalog_category_edit');
    Route::get('/admin/orders', 'CatalogController@orders_index');
    Route::post('/admin/catalog_orders_datatable', 'CatalogController@catalog_orders_datatable');
    Route::post('/admin/catalog_order_detele', 'CatalogController@catalog_order_detele');
    Route::get('/admin/orders/view_order/{id}', 'CatalogController@catalog_order_view');
    Route::get('/admin/profile', 'ProfileController@user_profile');

    //post
    Route::post('/admin/profile_save', 'ProfileController@profile_save');
    Route::post('/admin/catalog_category_save', 'CatalogController@catalog_category_save');
    Route::post('/admin/catalog_category_update/{id}', 'CatalogController@catalog_category_update');
    Route::post('/admin/catalog_item_save/{id}', 'CatalogController@catalog_item_save');
    Route::post('/admin/catalog_item_save', 'CatalogController@catalog_item_save');
    Route::post('/admin/upload_file/{directory_map}', 'FileController@upload_file');
});

Auth::routes();

